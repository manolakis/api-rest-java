package demo.service;

import org.manolakis.api.rest.test.AbstractTestCase;
import demo.model.User;
import demo.service.impl.UsersServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.reflect.Whitebox;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.util.ArrayList;

import static junit.framework.Assert.*;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * UsersService test case.
 */
public class UsersServiceTest extends AbstractTestCase {

    // - class attributes -------------------------------------------

    private final static String ID = "44e128a5-ac7a-4c9a-be4c-224b6bf81b20";

    // - instance attributes ----------------------------------------

    @Mock
    private DataSource dataSource;

    @Mock
    private ResultSet resultSet;

    @Mock
    private JdbcTemplate jdbcTemplate;

    @InjectMocks
    private UsersServiceImpl usersService;

    private RowMapper<User> rowMapper;

    // - instance methods -------------------------------------------

    @Before
    public void prepareTests() {
        this.usersService.setDataSource(this.dataSource);
        Whitebox.setInternalState(this.usersService, "jdbcTemplate", this.jdbcTemplate);
        this.rowMapper = Whitebox.getInternalState(UsersServiceImpl.class, "rowMapper");
    }

    @Test
    public void testRowMapper() throws Exception {

        when(this.resultSet.getString("id")).thenReturn(ID);
        when(this.resultSet.getString("name")).thenReturn("Manuel");
        when(this.resultSet.getString("email")).thenReturn("manuel@fakemail.com");
        when(this.resultSet.getBoolean("active")).thenReturn(true);

        final User user = this.rowMapper.mapRow(this.resultSet, 0);

        assertEquals(ID, user.getID().toString());
        assertEquals("Manuel", user.getName());
        assertEquals("manuel@fakemail.com", user.getEmail());
        assertTrue(user.isActive());
    }

    @Test
    public void testGetUsers() {
        final String QUERY = "select * from users";

        when(this.jdbcTemplate.query(QUERY, this.rowMapper)).thenReturn(new ArrayList<User>());

        assertEquals(0, this.usersService.get().size());
    }

    @Test
    public void testGetUser() {
        final String QUERY = "select * from users where id = ?";
        final User user = new User();

        user.setID(ID);
        user.setName("Manuel");
        user.setEmail("manuel@fakemail.com");
        user.setActive(true);

        when(this.jdbcTemplate.queryForObject(QUERY, rowMapper, ID)).thenReturn(user);

        assertNull(this.usersService.get(null));
        assertNull(this.usersService.get("other-ID"));
        assertEquals(ID, this.usersService.get(ID).getID());
    }

    @Test
    public void testUpdate() {
        final String QUERY = "update users set name = ?, email = ?, active = ? where id = ?";
        final User user = new User();

        user.setID(ID);
        user.setName("Manuel");
        user.setEmail("manuel@fakemail.com");
        user.setActive(true);

        when(this.jdbcTemplate.update(QUERY, user.getName(), user.getEmail(), user.isActive(), user.getID())).thenReturn(1);

        assertTrue(this.usersService.update(user));
        user.setID("other-ID");
        assertFalse(this.usersService.update(user));
    }

    @Test
    public void testCreate() {
        final String QUERY = "insert into users (id, name, email, active) values (?, ?, ?, ?)";
        final User user = new User();

        user.setID(ID);
        user.setName("Manuel");
        user.setEmail("manuel.martin@gmail.com");
        user.setActive(true);

        when(this.jdbcTemplate.update(QUERY, user.getID(), user.getName(), user.getEmail(), user.isActive())).thenReturn(1);

        assertTrue(this.usersService.create(user));
        user.setID("other-ID");
        assertFalse(this.usersService.create(user));
    }

    @Test
    public void testDelete() {
        final String QUERY = "delete from users where id = ?";
        final User user = new User();

        user.setID(ID);

        when(this.jdbcTemplate.update(QUERY, user.getID())).thenReturn(1);

        assertTrue(this.usersService.delete(user));
        user.setID("other-ID");
        assertFalse(this.usersService.delete(user));
    }

}
