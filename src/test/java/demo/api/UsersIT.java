package demo.api;

import demo.model.User;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 * Users integration tests.
 */
public class UsersIT {

    // - instance methods -------------------------------------------

    @Test
    public void testUsers() {
        RestTemplate restTemplate = new RestTemplate();
        final String usersURL = "http://127.0.0.1:8082/api/users";

        // lista de usuarios
        List result = restTemplate.getForObject(usersURL, List.class);
        assertNotNull(result);

        final int initialSize = result.size();

        // alta de usuario
        User user = new User();
        user.setName("Maria");
        user.setEmail("maria@fakemail.com");
        user.setActive(true);

        URI uri = restTemplate.postForLocation(usersURL, user);
        user = restTemplate.getForObject(uri, User.class);

        assertNotNull(user.getID());

        result = restTemplate.getForObject(usersURL, List.class);
        assertEquals(initialSize + 1, result.size());

        // modificación de usuario
        user.setName("Netolica");
        restTemplate.put(usersURL + "/{id}", user, user.getID());
        user = restTemplate.getForObject(usersURL + "/{id}", User.class, user.getID());

        assertEquals("Netolica", user.getName());

        // borrado de usuario
        restTemplate.delete(usersURL + "/{id}", user.getID());
        result = restTemplate.getForObject(usersURL, List.class);
        assertEquals(initialSize, result.size());
    }

}
