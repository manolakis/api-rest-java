package demo.api;

import org.manolakis.api.rest.test.AbstractTestCase;
import demo.model.User;
import demo.service.UsersService;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Users test case.
 */
public class UsersTest extends AbstractTestCase {

    // - class attributes -------------------------------------------

    private final static String ID = "44e128a5-ac7a-4c9a-be4c-224b6bf81b20";

    // - instance attributes ----------------------------------------

    @Mock
    private UsersService usersService;

    @InjectMocks
    private Users usersWS;

    // - instance methods -------------------------------------------

    @Test
    public void testGetUsers() {
        final List<User> users = new ArrayList<User>();

        when(usersService.get()).thenReturn(users);

        assertEquals(0, this.usersWS.getUsers().size());
    }

    @Test
    public void testGetUser() {
        when(usersService.get(ID)).thenReturn(new User());

        Response response = this.usersWS.getUser(ID);
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        assertNotNull(response.getEntity());

        response = this.usersWS.getUser("other");
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
        assertNull(response.getEntity());
    }

    @Test
    public void testCreateUser() {
        final User user1 = new User(),
                user2 = new User();

        when(usersService.create(user1)).thenReturn(true);
        when(usersService.create(user2)).thenReturn(false);

        Response response = this.usersWS.createUser(user1);
        assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
        assertNotNull(response.getLocation());

        response = this.usersWS.createUser(user2);
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
        assertNull(response.getLocation());
    }

    @Test
    public void testUpdateUser() {
        final User user1 = new User(),
                user2 = new User();

        when(usersService.update(user1)).thenReturn(true);
        when(usersService.update(user2)).thenReturn(false);

        Response response =  this.usersWS.updateUser(ID, user1);
        assertEquals(Response.Status.NO_CONTENT.getStatusCode(), response.getStatus());
        assertNull(response.getEntity());

        response =  this.usersWS.updateUser(ID, user2);
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
    }

    @Test
    public void testDeleteUser() {
        this.usersWS.deleteUser(ID);
    }

}
