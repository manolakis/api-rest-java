package org.manolakis.api.rest.filter;

import demo.model.User;
import org.apache.cxf.jaxrs.ext.MessageContext;
import org.apache.cxf.jaxrs.model.OperationResourceInfo;
import org.junit.Before;
import org.junit.Test;
import org.manolakis.api.rest.test.AbstractTestCase;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import javax.ws.rs.HttpMethod;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * PartialResponseFilter unit tests.
 */
public class PartialResponseFilterTest extends AbstractTestCase {

    // - instance attributes ----------------------------------------

    @Mock
    private MessageContext messageContext;

    @Mock
    private UriInfo uriInfo;

    @Mock
    private OperationResourceInfo operationResourceInfo;

    @InjectMocks
    private PartialResponseFilter partialResponseFilter;

    // - instance methods -------------------------------------------

    @Before
    public void resetMocks() {
        Mockito.reset(this.messageContext, this.uriInfo, this.operationResourceInfo);
    }

    @Test
    public void testFilterWithoutFields() {
        // preparing the mocks
        MultivaluedMap<String, String> parameters = new MultivaluedHashMap<String, String>();

        when(this.operationResourceInfo.getHttpMethod()).thenReturn(HttpMethod.GET);
        when(this.uriInfo.getQueryParameters()).thenReturn(parameters);
        when(this.messageContext.getUriInfo()).thenReturn(this.uriInfo);

        Response response = Response.ok(getUserList()).build();

        response = this.partialResponseFilter.handleResponse(null, this.operationResourceInfo, response);

        List<?> userList = (List<?>) response.getEntity();

        assertTrue(userList.get(0) instanceof User);

    }

    @Test
    public void testNonGetHttpMethod() {
        // preparing the mocks
        MultivaluedMap<String, String> parameters = new MultivaluedHashMap<String, String>();

        parameters.putSingle("fields", "name,id");

        when(this.operationResourceInfo.getHttpMethod()).thenReturn(HttpMethod.POST);
        when(this.uriInfo.getQueryParameters()).thenReturn(parameters);
        when(this.messageContext.getUriInfo()).thenReturn(this.uriInfo);

        Response response = Response.ok(getUserList()).build();

        response = this.partialResponseFilter.handleResponse(null, this.operationResourceInfo, response);

        List<?> userList = (List<?>) response.getEntity();

        assertTrue(userList.get(0) instanceof User);
    }

    @Test
    public void testPartialResponseFlag() {
        // preparing the mocks
        MultivaluedMap<String, String> parameters = new MultivaluedHashMap<String, String>();

        parameters.putSingle("fields", "name,id");

        when(this.operationResourceInfo.getHttpMethod()).thenReturn(HttpMethod.GET);
        when(this.uriInfo.getQueryParameters()).thenReturn(parameters);
        when(this.messageContext.getUriInfo()).thenReturn(this.uriInfo);
        when(this.messageContext.get(PartialResponseFilter.PARTIAL_RESPONSE_FLAG)).thenReturn(true);

        Response response = Response.ok(getUserList()).build();

        response = this.partialResponseFilter.handleResponse(null, this.operationResourceInfo, response);

        List<?> userList = (List<?>) response.getEntity();

        assertTrue(userList.get(0) instanceof User);
    }

    @Test
    public void testCollectionFilter() {
        // preparing the mocks
        MultivaluedMap<String, String> parameters = new MultivaluedHashMap<String, String>();

        parameters.putSingle("fields", "name,id");

        when(this.operationResourceInfo.getHttpMethod()).thenReturn(HttpMethod.GET);
        when(this.uriInfo.getQueryParameters()).thenReturn(parameters);
        when(this.messageContext.getUriInfo()).thenReturn(this.uriInfo);

        Response response = Response.ok(getUserList()).build();

        response = this.partialResponseFilter.handleResponse(null, this.operationResourceInfo, response);

        List<?> userList = (List<?>) response.getEntity();

        assertTrue(userList.get(0) instanceof Map);

        Map<String, Object> elem = (Map<String, Object>) userList.get(0);
        Set<String> keySet = elem.keySet();

        assertTrue(keySet.contains("name"));
        assertTrue(keySet.contains("id"));
        assertFalse(keySet.contains("email"));
        assertFalse(keySet.contains("active"));
    }

    @Test
    public void testSingleFilter() {
        // preparing the mocks
        MultivaluedMap<String, String> parameters = new MultivaluedHashMap<String, String>();

        parameters.putSingle("fields", "name,id");

        when(this.operationResourceInfo.getHttpMethod()).thenReturn(HttpMethod.GET);
        when(this.uriInfo.getQueryParameters()).thenReturn(parameters);
        when(this.messageContext.getUriInfo()).thenReturn(this.uriInfo);

        Response response = Response.ok(getUser()).build();

        response = this.partialResponseFilter.handleResponse(null, this.operationResourceInfo, response);

        assertTrue(response.getEntity() instanceof Map);

        Map<String, Object> elem = (Map<String, Object>) response.getEntity();
        Set<String> keySet = elem.keySet();

        assertTrue(keySet.contains("name"));
        assertTrue(keySet.contains("id"));
        assertFalse(keySet.contains("email"));
        assertFalse(keySet.contains("active"));
    }

    private List<User> getUserList() {
        List<User> userList = new ArrayList<User>();

        userList.add(getUser());
        userList.add(getUser());

        return userList;
    }

    private User getUser() {
        return new User();
    }
}
