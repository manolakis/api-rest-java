package org.manolakis.api.rest.test;

import org.junit.Before;
import org.mockito.MockitoAnnotations;

/**
 * AbstractTestCase for Mockito initialization.
 */
public abstract class AbstractTestCase {

    // - instance methods -------------------------------------------

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }
}
