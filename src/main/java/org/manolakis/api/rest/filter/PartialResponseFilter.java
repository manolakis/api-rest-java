package org.manolakis.api.rest.filter;

import org.apache.cxf.jaxrs.ext.MessageContext;
import org.apache.cxf.jaxrs.ext.ResponseHandler;
import org.apache.cxf.jaxrs.model.OperationResourceInfo;
import org.apache.cxf.message.Message;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Service;

import javax.ws.rs.HttpMethod;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.util.*;

/**
 * Partial Response Filter.
 * Allows to give to developers just the information that they needs. A developer can select just the information an app
 * needs at a given time; it cuts down on bandwidth issues, which is important for mobile apps.
 * <p/>
 * Example:
 * <code>
 * /users?fields=name,age
 * </code>
 */
@Service("partialResponseFilter")
public class PartialResponseFilter implements ResponseHandler {

    // - class attributes -------------------------------------------

    /**
     * Insert this flag into MessageContext from a REST Service in order to avoid the filter execution.
     */
    public final static String PARTIAL_RESPONSE_FLAG = PartialResponseFilter.class.getName() + ".FLAG";
    private final static String FIELDS = "fields";
    private final static ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    // - instance attributes ----------------------------------------

    @Context
    private MessageContext messageContext;

    // - instance methods -------------------------------------------

    @Override
    public Response handleResponse(final Message message, final OperationResourceInfo operationResourceInfo, final Response response) {
        if (operationResourceInfo.getHttpMethod().equals(HttpMethod.GET)) {
            if (this.messageContext.get(PARTIAL_RESPONSE_FLAG) == null) {
                final Response.ResponseBuilder responseBuilder = Response.fromResponse(response);
                final List<String> fields = this.getFields();

                if (fields.size() > 0) {
                    final Object entity = response.getEntity();

                    if (entity instanceof Collection) { // collection of items
                        final List<Map<String, Object>> collection = new ArrayList<Map<String, Object>>();

                        for (Object item : (Collection) entity) {
                            collection.add(this.filter(item, fields));
                        }

                        responseBuilder.entity(collection);

                    } else { // single item
                        responseBuilder.entity(this.filter(entity, fields));
                    }
                }

                return responseBuilder.build();
            }
        }

        return response;
    }

    /**
     * Removes from Map all keys not contained in fields list.
     *
     * @param object Map to filter
     * @param fields list of permitted fields
     */
    private Map<String, Object> filter(Object object, List<String> fields) {
        final Map<String, Object> map = OBJECT_MAPPER.convertValue(object, Map.class);
        final String keys[] = map.keySet().toArray(new String[]{});

        for (String key : keys) {
            if (!fields.contains(key)) {
                map.remove(key);
            }
        }

        return map;
    }

    /**
     * Retrieves the list of fields that user wants.
     *
     * @return List of fields
     */
    private List<String> getFields() {
        final MultivaluedMap<String, String> parameters = this.messageContext.getUriInfo().getQueryParameters();
        final String fields = parameters.getFirst(FIELDS);

        return fields != null ? Arrays.asList(fields.split(",")) : new ArrayList<String>();
    }
}