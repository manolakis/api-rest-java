package demo.model;

import java.util.UUID;

public class User {

    // - instance attributes ----------------------------------------

    private String ID;
    private String name;
    private String email;
    private boolean active;

    // - instance methods -------------------------------------------

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(final boolean active) {
        this.active = active;
    }
}
