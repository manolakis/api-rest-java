package demo.service.impl;

import demo.model.User;
import demo.service.AbstractService;
import demo.service.UsersService;
import org.apache.cxf.jaxrs.ext.MessageContext;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.core.Context;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

@Repository
public class UsersServiceImpl extends AbstractService implements UsersService {

    private static final RowMapper<User> rowMapper = new RowMapper<User>() {
        @Override
        public User mapRow(ResultSet resultSet, int i) throws SQLException {
            final User user = new User();
            user.setID(resultSet.getString("id"));
            user.setName(resultSet.getString("name"));
            user.setEmail(resultSet.getString("email"));
            user.setActive(resultSet.getBoolean("active"));
            return user;
        }
    };

    @Override
    @Transactional(readOnly = true)
    public List<User> get() {
        final String QUERY = "select * from users";
        return this.jdbcTemplate.query(QUERY, rowMapper);
    }

    @Override
    @Transactional(readOnly = true)
    public User get(String id) {
        final String QUERY = "select * from users where id = ?";
        return this.jdbcTemplate.queryForObject(QUERY, rowMapper, id);
    }

    @Override
    @Transactional
    public boolean update(final User user) {
        final String QUERY = "update users set name = ?, email = ?, active = ? where id = ?";
        return this.jdbcTemplate.update(QUERY, user.getName(), user.getEmail(), user.isActive(), user.getID()) == 1;
    }

    @Override
    @Transactional
    public boolean create(final User user) {
        final String QUERY = "insert into users (id, name, email, active) values (?, ?, ?, ?)";
        return this.jdbcTemplate.update(QUERY, user.getID(), user.getName(), user.getEmail(), user.isActive()) == 1;
    }

    @Override
    @Transactional
    public boolean delete(User user) {
        final String QUERY = "delete from users where id = ?";
        return this.jdbcTemplate.update(QUERY, user.getID()) == 1;
    }
}
