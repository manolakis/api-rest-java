package demo.service;

import demo.model.User;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsersService {

    List<User> get();
    User get(final String id);
    boolean update(final User user);
    boolean create(final User user);
    boolean delete(final User user);
}
