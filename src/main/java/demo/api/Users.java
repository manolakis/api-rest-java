package demo.api;

import demo.model.User;
import demo.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;
import java.util.UUID;

@Path("/users")
@Service("users")
@Produces(MediaType.APPLICATION_JSON)
public class Users {

    // - instance attributes ----------------------------------------

    @Autowired
    private UsersService usersService;

    // - instance methods -------------------------------------------

    @GET
    public List<User> getUsers() {
        return this.usersService.get();
    }

    @GET
    @Path("{id}")
    public Response getUser(@PathParam("id") String userID) {
        final User user = this.usersService.get(userID);

        return (user != null)
                ? Response.ok(user).build()
                : Response.status(Response.Status.NOT_FOUND).build();
    }

    @PUT
    @Path("{id}")
    public Response updateUser(@PathParam("id") String userID, User user) {
        user.setID(userID);

        return (this.usersService.update(user))
                ? Response.status(Response.Status.NO_CONTENT).build()
                : Response.status(Response.Status.BAD_REQUEST).build();
    }

    @POST
    public Response createUser(User user) {
        user.setID(UUID.randomUUID().toString());

        return (this.usersService.create(user))
                ? Response.created(URI.create("/users/" + user.getID())).build()
                : Response.status(Response.Status.BAD_REQUEST).build();
    }

    @DELETE
    @Path("{id}")
    public Response deleteUser(@PathParam("id") String userID) {
        final User user = new User();
        user.setID(userID);

        return (this.usersService.delete(user))
                ? Response.status(Response.Status.NO_CONTENT).build()
                : Response.status(Response.Status.BAD_REQUEST).build();
    }


}
