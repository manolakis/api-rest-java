API REST en Java
================

Este proyecto tiene como objetivo facilitar el inicio de creación de un API REST en Java.

Maven
-----

Todo el ciclo de vida del proyecto está realizado con Maven. El proyecto se encuentra configurado para ejecutar test unitarios y test de integración.

### mvn compile

Realiza la compilación del proyecto

### mvn test

Realiza los pasos anteriores y ejecuta los test unitarios.

### mvn verify

Ejecuta los pasos anteriores y además:

 * verifica la cobertura de las clases del proyecto con los test unitarios. Si no  supera unos mínimos falla la construcción.
 * chequea el código fuente buscando errores comunes (findbugs)
 * levanta un servidor Jetty y ejecuta los test de intergación (*IT.java)
   
### mvn package

Ejecuta los pasos anteriores y genera el paquete .WAR para su despliegue en un servidor de aplicaciones.

### mvn jetty:run

Levanta un servidor Jetty para poder realizar pruebas manuales.

### mvn eclipse:eclipse

Genera los archivos de proyecto de Eclipse. Una vez generados los archivos de proyecto éste puede ser importado en Eclipse.

Es recomendable ejecutar este comando cada vez que añadamos una nueva librería como dependencia en el pom.xml.