<?xml version="1.0" encoding="utf-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <!-- POM Relationships -->
    <groupId>com.martinprieto</groupId>
    <artifactId>api-rest-java</artifactId>
    <version>0.1-SNAPSHOT</version>
    <packaging>war</packaging>

    <!-- Project information -->
    <name>API REST Java</name>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <java.version>1.6</java.version>

        <!-- Plugins versions -->
        <cobertura-maven-plugin.version>2.5.1</cobertura-maven-plugin.version>
        <findbugs-maven-plugin.version>2.4.0</findbugs-maven-plugin.version>
        <jetty-maven-plugin.version>8.1.5.v20120716</jetty-maven-plugin.version>
        <maven-checkstyle-plugin.version>2.9.1</maven-checkstyle-plugin.version>
        <maven-compiler-plugin.version>2.3.2</maven-compiler-plugin.version>
        <maven-failsafe-plugin.version>2.6</maven-failsafe-plugin.version>
        <maven-surefire-plugin.version>2.12</maven-surefire-plugin.version>

        <!-- Dependencies versions -->
        <c3p0.version>0.9.1.2</c3p0.version>
        <cxf.version>2.7.0</cxf.version>
        <h2.version>1.3.168</h2.version>
        <jackson.version>1.9.10</jackson.version>
        <junit.version>4.10</junit.version>
        <liquibase.version>2.0.5</liquibase.version>
        <mockito.version>1.9.0</mockito.version>
        <powermock.version>1.4.12</powermock.version>
        <spring.version>3.1.2.RELEASE</spring.version>
    </properties>

    <build>
        <plugins>
            <!-- JDK 1.6 code. Shows warnings and deprecated code in compilation. -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>${maven-compiler-plugin.version}</version>
                <configuration>
                    <source>${java.version}</source>
                    <target>${java.version}</target>
                    <showDeprecation>true</showDeprecation>
                    <showWarnings>true</showWarnings>
                </configuration>
            </plugin>

            <!-- Jetty server for integration tests -->
            <plugin>
                <groupId>org.mortbay.jetty</groupId>
                <artifactId>jetty-maven-plugin</artifactId>
                <version>${jetty-maven-plugin.version}</version>
                <configuration>
                    <stopPort>8005</stopPort>
                    <stopKey>STOP</stopKey>
                    <scanIntervalSeconds>120</scanIntervalSeconds>
                    <useTestClasspath>true</useTestClasspath>
                    <webAppConfig>
                        <contextPath>/</contextPath>
                    </webAppConfig>
                    <connectors>
                        <connector implementation="org.eclipse.jetty.server.nio.SelectChannelConnector">
                            <port>8082</port>
                            <maxIdleTime>30000</maxIdleTime>
                            <acceptors>2</acceptors>
                        </connector>
                    </connectors>
                </configuration>
                <executions>
                    <execution>
                        <id>start-jetty</id>
                        <phase>pre-integration-test</phase>
                        <goals>
                            <goal>run</goal>
                        </goals>
                        <configuration>
                            <scanIntervalSeconds>0</scanIntervalSeconds>
                            <daemon>true</daemon>
                        </configuration>
                    </execution>
                    <execution>
                        <id>stop-jetty</id>
                        <phase>post-integration-test</phase>
                        <goals>
                            <goal>stop</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <!-- ========================
                QA Plugins configuration
            ========================= -->

            <!-- FailSafe for integration tests execution -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-failsafe-plugin</artifactId>
                <version>2.12.4</version>
                <configuration>
                    <argLine>-Xms256m -Xmx512m -XX:PermSize=512m -XX:MaxPermSize=512m</argLine>
                </configuration>
                <executions>
                    <execution>
                        <goals>
                            <goal>integration-test</goal>
                            <goal>verify</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <!-- FindBugs plugin -->
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>findbugs-maven-plugin</artifactId>
                <version>${findbugs-maven-plugin.version}</version>
                <configuration>
                    <threshold>High</threshold>
                    <xmlOutput>true</xmlOutput>
                </configuration>
                <executions>
                    <execution>
                        <phase>verify</phase>
                        <goals>
                            <goal>check</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <!-- CheckStyle: uncomment for static code analysis
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-checkstyle-plugin</artifactId>
                <version>${maven-checkstyle-plugin.version}</version>
                <configuration>
                    <consoleOutput>true</consoleOutput>
                    <failOnViolation>true</failOnViolation>
                    <includeTestSourceDirectory>false</includeTestSourceDirectory>
                </configuration>
                <executions>
                    <execution>
                        <phase>verify</phase>
                        <goals>
                            <goal>check</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            -->

            <!-- Test source coverage -->
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>cobertura-maven-plugin</artifactId>
                <version>${cobertura-maven-plugin.version}</version>
                <configuration>
                    <check>
                        <haltOnFailure>true</haltOnFailure>
                        <branchRate>85</branchRate>
                        <lineRate>85</lineRate>
                        <totalBranchRate>85</totalBranchRate>
                        <totalLineRate>85</totalLineRate>
                        <packageLineRate>85</packageLineRate>
                        <packageBranchRate>85</packageBranchRate>
                    </check>
                </configuration>
                <executions>
                    <execution>
                        <phase>verify</phase>
                        <goals>
                            <goal>clean</goal>
                            <goal>check</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <!-- Surefire to launch test -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>${maven-surefire-plugin.version}</version>
                <configuration>
                    <!-- More memory to avoid PermGen Exception -->
                    <argLine>-Xms256m -Xmx512m -XX:PermSize=512m -XX:MaxPermSize=512m</argLine>
                </configuration>
            </plugin>

            <!-- =========================
                IDE Plugins configuration  
            ========================== -->

            <!-- Eclipse Integration -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-eclipse-plugin</artifactId>
                <configuration>
                    <wtpContextName>${project.artifactId}</wtpContextName>
                    <additionalProjectFacets>
                        <jst.java>${java.version}</jst.java>
                        <jst.web>3.0</jst.web>
                    </additionalProjectFacets>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <!-- Dependencies information -->
    <dependencies>

        <!-- ===================
           Compile dependencies
        ==================== -->

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-core</artifactId>
            <version>${spring.version}</version>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
            <version>${spring.version}</version>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-beans</artifactId>
            <version>${spring.version}</version>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-jdbc</artifactId>
            <version>${spring.version}</version>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-web</artifactId>
            <version>${spring.version}</version>
        </dependency>

        <dependency>
            <groupId>org.apache.cxf</groupId>
            <artifactId>cxf-rt-frontend-jaxrs</artifactId>
            <version>${cxf.version}</version>
        </dependency>

        <dependency>
            <groupId>org.codehaus.jackson</groupId>
            <artifactId>jackson-jaxrs</artifactId>
            <version>${jackson.version}</version>
        </dependency>

        <dependency>
            <groupId>org.liquibase</groupId>
            <artifactId>liquibase-core</artifactId>
            <version>${liquibase.version}</version>
        </dependency>

        <dependency>
            <groupId>c3p0</groupId>
            <artifactId>c3p0</artifactId>
            <version>${c3p0.version}</version>
        </dependency>

        <!-- Database Driver: must be changed by the properly database driver -->
        <dependency>
            <groupId>com.h2database</groupId>
            <artifactId>h2</artifactId>
            <version>${h2.version}</version>
        </dependency>

        <!-- =================
            Test dependencies  
        ================== -->

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
            <version>${spring.version}</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>${junit.version}</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-core</artifactId>
            <version>${mockito.version}</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.powermock</groupId>
            <artifactId>powermock-core</artifactId>
            <version>${powermock.version}</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.powermock</groupId>
            <artifactId>powermock-api-mockito</artifactId>
            <version>${powermock.version}</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.powermock</groupId>
            <artifactId>powermock-module-junit4</artifactId>
            <version>${powermock.version}</version>
            <scope>test</scope>
        </dependency>

        <!-- =====================
            Provided dependencies  
        ====================== -->

        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>javax.servlet-api</artifactId>
            <version>3.0.1</version>
            <scope>provided</scope>
        </dependency>

    </dependencies>

</project>